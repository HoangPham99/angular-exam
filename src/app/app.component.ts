import { Component, OnInit } from '@angular/core';

import { SharedService } from './service/shared.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
   userEmail : string;

  constructor(private shareService: SharedService) {

  }


  ngOnInit(): void {
    this.shareService.changeEmitted$.subscribe(text => {
      this.userEmail = text;
      console.log(text);
    }); 
  }



  title = 'project1';
}
