import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {MDBBootstrapModule} from 'angular-bootstrap-md'
import {RedirectModule} from './modul/redirect.module'
import {FormsModule} from '@angular/forms'
import {HttpClientModule} from '@angular/common/http'

import { AppComponent } from './app.component';
import {NavTopComponent} from './component/nav-top/nav-top.component'
import {FooterBottomComponent} from './component/footer-bottom/footer-bottom.component'
import {ListInterviewerComponent} from './component/list-interviewer/list-interviewer.component'
import {ProfileComponent} from './component/profile/profile.component';
import { PageErrorComponent } from './component/page-error/page-error.component';
import { LoginComponent } from './component/login/login.component';
import { HomeComponent } from './component/home/home.component'


@NgModule({
  declarations: [
    AppComponent,
    NavTopComponent,
    FooterBottomComponent,
    ListInterviewerComponent,
    ProfileComponent,
    PageErrorComponent,
    LoginComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,MDBBootstrapModule.forRoot(),RedirectModule,FormsModule,HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
