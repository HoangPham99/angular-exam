import { Component, OnInit } from '@angular/core';
import {SheduleService} from '../../service/shedule.service'
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {



  constructor(private shedule:SheduleService) {}

  ngOnInit() {
      this.shedule.getScheduler().subscribe(data => {
        console.log(data);
        
      });

  }

}
