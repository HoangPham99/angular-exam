import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SharedService } from '../../service/shared.service';
import { LoginService } from '../../service/login.service'


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {



  email = '';
  password = '';

  isLogin = false;
  listUser = [
    { email: 'pqhoang@gmail.com', password: 'Hoang123' },
    { email: 'hnhvy@gamil.com', password: 'Hoang123' },
    { email: 'ngTrong@gamil.com', password: 'Hoang123' },
    { email: 'admin@gamil.com', password: 'Hoang123' },
  ];
  constructor(private router: Router, private shareService: SharedService, private loginService: LoginService) { }

  ngOnInit() {
  }
  routerAndSetName() {
    console.log('Login thanh cong');
    this.isLogin = false;
    this.router.navigate(['home']);
    this.shareService.emitChange(this.email);
  }



  onFocus() {
    this.isLogin = false;
  }
  checkLogin(event) {
    this.email = event.value.email;
    this.password = event.value.password;
    let postData = { username: this.email, password: this.password };
    // console.log(this.postData);

    this.loginService.postUser(postData).subscribe(data => {
        if(data){
          this.routerAndSetName();
        }else {
          this.isLogin =true;
        }
      
    },error => { 
      this.isLogin = true;

      console.log(error.status);
    });


    // let check = false;
    // for (let key in this.listUser) {
    //   if ((this.email === this.listUser[key].email) && (this.password === this.listUser[key].password)) {
    //     this.routerAndSetName();
    //     break;
    //   } else {
    //     this.isLogin = true;
    //   }
    // }
  }



}
