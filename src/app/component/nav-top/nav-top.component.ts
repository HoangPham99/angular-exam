import { Component, OnInit,Input} from '@angular/core';


@Component({
  selector: 'app-nav-top',
  templateUrl: './nav-top.component.html',
  styleUrls: ['./nav-top.component.scss']
})
export class NavTopComponent implements OnInit {
  @Input() emailUser: string = '';

  isLogin = true;

  constructor() { 
    
  }

  ngOnInit() {
  }

}
