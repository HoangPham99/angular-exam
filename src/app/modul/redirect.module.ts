import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router'

import { ListInterviewerComponent } from '../component/list-interviewer/list-interviewer.component'
import { ProfileComponent } from '../component/profile/profile.component'
import { PageErrorComponent } from '../component/page-error/page-error.component'
import {HomeComponent} from '../component/home/home.component'
import {LoginComponent} from '../component/login/login.component'

const appRoute: Routes = [
    {path:'login',component: LoginComponent},
    { path: 'home', component: HomeComponent },
    { path: 'views', component: ListInterviewerComponent },
    { path: 'profiles', component: ProfileComponent },
    { path: '', redirectTo: '/login', pathMatch: "full" },
    { path: '**', component : PageErrorComponent }
];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoute)
    ],
    exports: [RouterModule]
})

export class RedirectModule {

}