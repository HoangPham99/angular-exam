import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http:HttpClient) { }

  
  
  postUser(body){
    return  this.http.post('http://192.168.78.114:8080/api/interviewer/authenticate', body);
  }

}
