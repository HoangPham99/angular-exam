import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'


@Injectable({
  providedIn: 'root'
})
export class SheduleService {

  constructor(private http:HttpClient) { }

    getScheduler(){
      return this.http.get('http:/http://192.168.78.113:8080/schedule-detail/');
    }
}
